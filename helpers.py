"""
    Import packages
"""
import time
import os
import io
import html

"""
    Formatted Log Function;
    Displays formatted message text in console, also allows for saving log output into log file
"""


def log(message, should_save=True):
    print(str(time.time()))
    print("\t" + str(message))

    if should_save == True:
        if not os.path.isfile("access.log"):
            with io.open("access.log", "w") as file:
                file.write(str(time.time()) + ": " + str(message) + "\n")
        else:
            with io.open("access.log", "a") as file:
                file.write(str(time.time()) + ": " + str(message) + "\n")


"""
    Ensures that the request describes a valid HTTP method and strips empty requests
"""


def recognised_method(_request):
    _req_string = str(_request.decode('utf-8'))

    # Strip empty requests
    if len(_req_string) < 4:
        return False

    if _req_string.find("GET") == -1 and _req_string.find("POST") == -1 and _req_string.find(
            "PUT") == -1 and _req_string.find("PATCH") == -1 and _req_string.find("DELETE") == -1:
        return False

    print(_req_string)
    return True


"""
    Extracts a significant amount of information about a request.
    The following are expected output on typical request
    {
        "method": (String) HTTP method in uppercase (e.g. GET POST etc...)
        "path": (String) HTTP url without GET params
    }
"""


def get_request_info(_request):
    _req_arr = str(_request.decode('utf-8')).split("\n")
    _output = {
        "method": _req_arr[0].split(" ")[0],
        "path": _req_arr[0].split(" ")[1].split("?")[0]
    }

    return _output


"""
    Process and return POST request body text
"""
def process_post_data(_request):
    log("Processing POST body data into List")
    _output = {}
    _input = _request.decode('utf-8').split("\r")[-1].split("\n")[1].split("&")
    for set in _input:
        _output[set.split("=")[0]] = html.unescape(set.split("=")[1])

    return _output