import re
from controller_handler import *

def handle_route (req):

    # Generic static & static-like routes to files and dynamically-generated files

    route_static_js = re.match('GET \/([A-Za-z0-9])\S+.js ', req.decode('utf-8'))
    if route_static_js: return controller_static(req, route_static_js, "text/javascript")

    route_static_css = re.match('GET \/([A-Za-z0-9])\S+.css ', req.decode('utf-8'))
    if route_static_css: return controller_static(req, route_static_css, "text/css")

    route_static_txt = re.match('GET \/([A-Za-z0-9])\S+.txt ', req.decode('utf-8'))
    if route_static_txt: return controller_static(req, route_static_txt, "text/plain")

    route_static_jpg = re.match('GET \/([A-Za-z0-9])\S+.jpg ', req.decode('utf-8')) or re.match('GET \/([A-Za-z])\w+.jpeg ', req.decode('utf-8'))
    if route_static_jpg: return controller_static(req, route_static_jpg, "image/jpeg")

    route_static_png = re.match('GET \/([A-Za-z0-9])\S+.png ', req.decode('utf-8'))
    if route_static_png: return controller_static(req, route_static_png, "image/png")

    route_static_map = re.match('GET \/([A-Za-z0-9])\S+.map ', req.decode('utf-8'))
    if route_static_map: return False



    # Custom route handlers for specific pages and actions

    route_index = re.match('GET /\sHTTP/1', req.decode('utf-8'))
    if route_index: return controller_get_index(req)

    route_test = re.match('GET /test\sHTTP/1', req.decode('utf-8'))
    if route_test: return controller_get_test(req)

    route_notify = re.match('GET /notify\?id=(\d+)\sHTTP/1', req.decode('utf-8'))
    if route_notify: return controller_get_notify(req, route_notify)

    route_dashboard = re.match('GET /dashboard\sHTTP/1', req.decode('utf-8'))
    if route_dashboard: return controller_get_dashboard(req)

    route_account = re.match('GET /account\sHTTP/1', req.decode('utf-8'))
    if route_account: return controller_get_account(req)

    route_sign_in = re.match('GET /signin\sHTTP/1', req.decode('utf-8'))
    if route_sign_in: return controller_get_sign_in(req)

    route_post_sign_in = re.match('POST /signin\sHTTP/1', req.decode('utf-8'))
    if route_post_sign_in: return controller_post_sign_in(req)

    route_sign_out = re.match('GET /signout\sHTTP/1', req.decode('utf-8'))
    if route_sign_out: return controller_get_sign_out(req)

    route_post_changepassword = re.match('POST /changepassword\sHTTP/1', req.decode('utf-8'))
    if route_post_changepassword: return controller_post_changepassword(req)

    route_post_adduser = re.match('POST /adduser\sHTTP/1', req.decode('utf-8'))
    if route_post_adduser: return controller_post_adduser(req)

    route_post_sendreceipt = re.match('POST /send_receipt\sHTTP/1', req.decode('utf-8'))
    if route_post_sendreceipt: return controller_post_sendreceipt(req)

    route_get_allclients = re.match('POST /send_receipt\sHTTP/1', req.decode('utf-8'))
    if route_get_allclients: return controller_get_allclients(req)
