import json
import io
import re



def json_from_list(_list):
    return ("""HTTP/1.0 200 OK
Content-Type: application/json

"""+json.dumps(_list)).encode('utf-8')



def json_from_dictionary(_dictionary):
    return ("""HTTP/1.0 200 OK
Content-Type: application/json

"""+json.dumps(_dictionary)).encode('utf-8')




def html_from_file(_path):
    with io.open("html\\"+_path, "rb") as file:
        _output = ""
        _output += "HTTP/1.0 200 OK\n"
        _output += "Content-Type: text/html\n\n"
        _output += file.read()
        return (_output).encode('utf-8')



def html_from_template(_path, _data):
    with io.open("html\\"+_path, "r") as file:
        _output = ""
        _output += "HTTP/1.0 200 OK\n"
        _output += "Content-Type: text/html\n\n"

        # Templating
        _template = re.split('{{{|}}}', file.read())
        for index, item in enumerate(_template):
            """
                odd number. If two delimiters are used properly, all array items with odd indexes should be templating
                tags intended of substitution with real-time data
            """
            if index % 2 != 0:
                _template[index] = _data[item]

        _output += "".join(_template)


        return (_output).encode('utf-8')



def redirect_to_url(_redirect_url):
    return html_from_template("redirect.template.html", {
        'redirect_url': _redirect_url
    })



