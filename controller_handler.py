import io
import sys
from content_generators import \
    json_from_list, json_from_dictionary, html_from_file, html_from_template, redirect_to_url
from actions import *
from helpers import log, get_request_info, process_post_data

"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']


# Generic static & static-like routes to files and dynamically-generated files

def controller_static(req, params, mime_type):
    try:
        with io.open("html\\" + params.group(0).split("GET /")[1], "rb") as file:
            _output = "HTTP/1.0 200 OK\n"
            _output += "Content-Type: " + mime_type + "\n\n"
            _output = _output.encode('utf-8')
            _output += file.read()
            return _output
    except FileNotFoundError:
        return False


# Custom UI-type controller handlers for specific pages and actions

def controller_get_index(req):
    if is_signed_in():
        return redirect_to_url("/dashboard")
    else:
        return redirect_to_url("/signin")


def controller_get_test(req):
    _output = {
        "status": "ok"
    }
    return json_from_dictionary(_output)


def controller_get_notify(req, params):
    _info = get_request_info(req)
    _id = params.group(1)
    _client = get_client(_id)

    return html_from_template("notify.template.html", {
        'id': _id,
        'username': _client['username'],
        'bitcoin_address': main.cc_bitcoin_address
    })


def controller_get_dashboard(req):
    if is_signed_in():
        return html_from_template("dashboard.template.html", {
            'username': main.cc_current_user['username']
        })
    else:
        return redirect_to_url("/")


def controller_get_account(req):
    if is_signed_in():
        return html_from_template("account.template.html", {
            'username': main.cc_current_user['username']
        })
    else:
        return redirect_to_url("/")


def controller_get_sign_in(req):
    return html_from_template("signin.template.html", {})


def controller_post_sign_in(req):
    _input = process_post_data(req)

    if sign_in(_input) == True:
        return json_from_dictionary({
            'status': 'ok'
        })
    else:
        return json_from_dictionary({
            'status': 'error',
            'error_msg': "Incorrect username or password!"
        })


def controller_get_sign_out(req):
    sign_out()
    return redirect_to_url("/")


def controller_post_changepassword(req):
    _input = process_post_data(req)

    if changepassword(_input) == True:
        return json_from_dictionary({
            'status': 'ok'
        })
    else:
        return json_from_dictionary({
            'status': 'error',
            'error_msg': "New password could not be set!"
        })


def controller_post_adduser(req):
    _input = process_post_data(req)

    if (adduser(_input) == True) and (main.cc_current_user is not None):
        return json_from_dictionary({
            'status': 'ok'
        })
    else:
        return json_from_dictionary({
            'status': 'error',
            'error_msg': "New user failed to be added! Username & Password should be only alpha-numeric..."
        })


def controller_post_sendreceipt(req):
    _input = process_post_data(req)

    _process = process_receipt(_input)
    if (_process):
        return json_from_dictionary({
            'status': 'ok',
            'data':{
                'identifier': _process
            }
        })
    else:
        return json_from_dictionary({
            'status': 'error',
            'error_msg': "Receipt could not be received!"
        })\


def controller_get_allclients(req):
    with db_connect() as conn:
        _clients = db_getallclients()
        return json_from_dictionary({
            'status': 'ok',
            'data': _clients
        })