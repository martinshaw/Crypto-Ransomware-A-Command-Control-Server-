import io
import sys
import urllib.parse

from helpers import log
from database import *

"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']


def is_signed_in():
    if main.cc_current_user is None:
        return False
    else:
        return True


def sign_in(credentials):
    log(credentials['username'] + " is attempting to sign in ...")
    with db_connect() as conn:
        if db_authenticate(conn, credentials['username'], credentials['password']) == True:
            log("Sign in attempt succeeded!!!")
            main.cc_current_user = credentials
            return True
        else:
            log("Sign in attempt failed!!!")
            main.cc_current_user = None
            return False


def sign_out():
    main.cc_current_user = None
    return True


def changepassword(passwords):
    log(main.cc_current_user['username'] + " is attempting to change password ...")
    with db_connect() as conn:
        if db_changepassword(conn, main.cc_current_user['username'], passwords['old_password'], passwords['new_password']) == True:
            log("Password change attempt succeeded!!!")
            main.cc_current_user['password'] = passwords['new_password']
            return True
        else:
            log("Password change attempt failed!!!")
            return False


def adduser(credentials):
    log(main.cc_current_user['username'] + " is attempting to add a new user with the username '"+credentials['username']+"' ...")
    with db_connect() as conn:
        if db_adduser(conn, credentials['username'], credentials['password']) == True:
            log("New user addition attempt succeeded!!!")
            return True
        else:
            log("New user addition attempt failed!!!")
            return False



def process_receipt(receipt):
    log("Attempting to process receipt...")
    with db_connect() as conn:
        _query = db_addclient(conn, urllib.parse.unquote(receipt['username']), urllib.parse.unquote(receipt['keyset']), urllib.parse.unquote(str(receipt['files'])))
        if _query:
            log("New client added from receipt!!!")
            return _query
        else:
            log("Client addition attempt failed!!!")
            return False



def get_client(id):
    log("Attempting to get client data by ID...")
    with db_connect() as conn:
        _query = db_getclient(conn, id)
        if _query:
            log("Found Client!")
            return _query
        else:
            log("Did not find Client!")
            return False