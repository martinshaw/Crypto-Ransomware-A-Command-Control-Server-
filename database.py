import sqlite3


def db_connect():
    """
        Create a database connection to the SQLite database
    """
    try:
        conn = sqlite3.connect('cmdcntl.db')
        return conn
    except sqlite3.Error as e:
        print(e)

    return None


def db_authenticate(conn, username, password):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password,))
    rows = cursor.fetchall()

    try:
        if not rows[0]:
            return False
        else:
            db_user = rows[0][1]
            db_pass = rows[0][2]

            if (username == db_user) and (password == db_pass):
                return True
            else:
                return False

    except IndexError as e:
        return False


def db_changepassword(conn, username, old_password, new_password):
    cursor = conn.cursor()
    cursor.execute("UPDATE users SET password=? WHERE username=? AND password=?",
                   (new_password, username, old_password,))
    rows = cursor.fetchall()

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=?", (username,))
    rows = cursor.fetchall()

    try:
        if not rows[0]:
            return False
        else:
            db_pass = rows[0][2]

            if (new_password == db_pass):
                return True
            else:
                return False

    except IndexError as e:
        return False


def db_adduser(conn, username, password):
    if username == "" or password == "" or not username.isalnum() or not password.isalnum():
        return False

    cursor = conn.cursor()
    cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (username, password,))
    rows = cursor.fetchall()

    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password,))
    rows = cursor.fetchall()

    try:
        if not rows[0]:
            return False
        else:
            return True

    except IndexError as e:
        return False


def db_addclient(conn, username, keyset, files):
    cursor = conn.cursor()
    cursor.execute("INSERT INTO clients (username, keyset, files) VALUES (?, ?, ?)", (username, keyset, files))
    rows = cursor.fetchall()

    cursor = conn.cursor()
    cursor.execute("SELECT id FROM clients WHERE keyset=?", (keyset,))
    rows = cursor.fetchall()

    try:
        if not rows[0]:
            return False
        else:
            print("id = " + str(rows[0][0]))
            return rows[0][0]

    except IndexError as e:
        return False


def db_getclient(conn, id):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM clients WHERE id=?", (id,))
    rows = cursor.fetchall()

    try:
        if not rows[0]:
            return False
        else:
            print("Getting client data from database: " + str(rows[0]))
            return {
                'id': rows[0][0],
                'username': rows[0][1],
                'keyset': rows[0][2],
                'files': rows[0][3]
            }

    except IndexError as e:
        return False


def db_getallclients(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM clients")
    rows = cursor.fetchall()

    return rows
