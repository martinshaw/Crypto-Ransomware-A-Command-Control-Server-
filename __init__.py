"""
    Import packages
"""
import socket
from helpers import log, recognised_method
from routes_handler import handle_route



"""
    Initialise user-specific variables
"""
cc_current_user = None




"""
    Initialize hostname and sockets and other variables
"""
cc_web_host, cc_web_port = '', 9999           # Will likely use IP address while testing and demonstrating. Also reduces casual identification
cc_bitcoin_address = "196ygS5VRjE2QdNrjjhJRZv552QPLZxgBr"


"""
    Start web server by attaching to port and listening for requests on that port
"""
cc_web_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cc_web_sock.bind( (cc_web_host, cc_web_port) )
cc_web_sock.listen(1)                  # Only allow one concurrent request to be listened for and accepted in any one duration of time



"""
    Socket event loop.
    Forever listening for Socket connection requests to accept until script is terminated
"""
while True:
    csock, caddr = cc_web_sock.accept()
    log("Accepted connection from " + str(caddr))
    req = csock.recv(1024)      # Take first 1kb of raw request data as bytestream

    try:                                    # Don't rage quit after one bad request. Quietly give up, log for later fix. Take another request.
        if recognised_method(req):
            _response = handle_route(req)
            if not _response:                   # Not routes (regular expressions) matched. Throw 404 error page
                csock.sendall("HTTP/1.0 404 Not Found\r\n".encode('utf-8'))
            else:                               # Route (reg. ex.) has matched. Use this corresponding controller's output as response
                csock.sendall(_response)
    except Exception as e:
        log("Execeptional Error: " + str(e))

    csock.close()